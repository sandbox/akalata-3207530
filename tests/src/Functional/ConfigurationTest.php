<?php

namespace Drupal\Tests\tag1quo\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Tag1Quo configuration form.
 *
 * @group tag1quo
 */
class ConfigurationTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tag1quo'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * @todo Remove after https://support.tag1.com/issues/39940.
   *
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Tests configuration values are saved.
   */
  public function testConfigurationForm() {
    $user = $this->drupalCreateUser(['administer tag1quo']);
    $this->drupalLogin($user);

    $this->drupalGet('admin/config/development/tag1quo');
    $this->assertSession()->pageTextContainsOnce('No information will be sent to Tag1 Quo until you configure your token.');
    $this->assertSession()->checkboxNotChecked('debug[enabled]');

    // Assert the configuration form can be updated.
    $this->submitForm(['debug[enabled]' => TRUE], t('Save configuration'));
    $this->assertSession()->checkboxChecked('debug[enabled]');
  }

}
